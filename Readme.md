# Django & openAI Code Creation

Easy and quick development where we include openAI library to create, modify and fix code in different programming languages.
Also, we can copy result and paste it wherever is required.

This project is based in:
- Python 3.10.9
- Django
- openAI

Libraries and others:
- Django==3.2.5
- openAI==0.28.0
- Bootstrap: https://getbootstrap.com/
- Prism.js: https://prismjs.com/
- openAI: https://openai.com/
- openAI documentation: https://platform.openai.com/docs/guides/text-generation

Steps procedure:
- Define quantity of apps
- Define and design quantity of html files to use
- Link all html files
- Include base file
- Download prism files
- Designing home.html page
- Including openAI feature
- Adding 'Suggest' page
- Include validation for openai_key
- Include login, logout and register user (user authentication)
- Save output in database
- Include 'past code' page and historical data displayed on screen
- Create 'delete' feature