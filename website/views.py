from django.shortcuts import render, redirect
from django.contrib import messages
import openai
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from .forms import SignUpForm
from .models import Code

openai_key = "no-key"


def home(request):
    lang_list = ['c', 'clike', 'cpp', 'csharp', 'css', 'dart', 'django', 'go', 'java', 'javascript', 'markup',
                 'markup-templating', 'matlab', 'mongodb', 'objectivec', 'perl', 'php', 'powershell', 'python', 'r',
                 'regex', 'robotframework', 'ruby', 'rust', 'sass', 'scala', 'sql', 'swift', 'yaml']

    if request.method == "POST":
        code = request.POST['code']
        lang = request.POST['lang']

        # Check that a language was selected

        if lang == "Select programming language":
            messages.success(request, "Please, select a programming language.")

            return render(request, 'website/home.html',
                          {'lang_list': lang_list, 'code': code, 'lang': lang})

        if openai_key == "no-key":
            messages.success(request, "Please, include an openai_key!!\nFollow steps in "
                                      "https://platform.openai.com/docs/guides/text-generation")
            return render(request, 'website/home.html',
                          {'lang_list': lang_list, 'code': code, 'lang': lang})

        # openAI
        openai.api_key = openai_key

        # Create openAI instance
        openai.Model.list()

        # Make an openai request
        try:
            response = openai.Completion.create(
                engine='gpt-3.5-turbo-instruct',
                prompt=f"Respond only with code. Fix this {lang} code: {code}",
                temperature=0,
                max_tokens=1000,
                top_p=1.0,
                frequency_penalty=0.0,
                presence_penalty=0.0,
            )

            # Parse the response
            response = (response["choices"][0]["text"]).strip()

            # Save to database
            record = Code(question=code, code_answer=response, language=lang, user=request.user)
            record.save()

            return render(request, 'website/home.html',
                          {'lang_list': lang_list, 'response': response, 'lang': lang})

        except Exception as msg:
            return render(request, 'website/home.html',
                          {'lang_list': lang_list, 'response': msg, 'lang': lang})

    return render(request, 'website/home.html', {'lang_list': lang_list})


def suggest(request):
    lang_list = ['c', 'clike', 'cpp', 'csharp', 'css', 'dart', 'django', 'go', 'java', 'javascript', 'markup',
                 'markup-templating', 'matlab', 'mongodb', 'objectivec', 'perl', 'php', 'powershell', 'python', 'r',
                 'regex', 'robotframework', 'ruby', 'rust', 'sass', 'scala', 'sql', 'swift', 'yaml']

    if request.method == "POST":
        code = request.POST['code']
        lang = request.POST['lang']

        # Check that a language was selected

        if lang == "Select programming language":
            messages.success(request, "Please, select a programming language.")

            return render(request, 'website/suggest.html',
                          {'lang_list': lang_list, 'code': code, 'lang': lang})

        if openai_key == "no-key":
            messages.success(request, "Please, include an openai_key!!\nFollow steps in "
                                      "https://platform.openai.com/docs/guides/text-generation")
            return render(request, 'website/home.html',
                          {'lang_list': lang_list, 'code': code, 'lang': lang})

        # openAI
        openai.api_key = openai_key

        # Create openAI instance
        openai.Model.list()

        # Make an openai request
        try:
            response = openai.Completion.create(
                engine='gpt-3.5-turbo-instruct',
                prompt=f"Respond only with code. {code}",
                temperature=0,
                max_tokens=1000,
                top_p=1.0,
                frequency_penalty=0.0,
                presence_penalty=0.0,
            )

            # Parse the response
            response = (response["choices"][0]["text"]).strip()
            return render(request, 'website/suggest.html',
                          {'lang_list': lang_list, 'response': response, 'lang': lang})

        except Exception as msg:
            return render(request, 'website/suggest.html',
                          {'lang_list': lang_list, 'response': msg, 'lang': lang})

    return render(request, 'website/suggest.html', {'lang_list': lang_list})


def login_user(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            messages.success(request, "You have been logged in")
            return redirect('home')
        else:
            messages.success(request, "Error logging in. Please, try again...")
            return redirect('home')
    else:
        return render(request, 'website/home.html', {})


def logout_user(request):
    logout(request)
    messages.success(request, "You have been logged out")
    return redirect('home')


def register_user(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)

        if form.is_valid():
            form.save()

            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = authenticate(username=username, password=password)
            login(request, user)

            messages.success(request, "You have registered")
            return redirect('home')
    else:
        form = SignUpForm()

    return render(request, 'website/register.html', {"form": form})


def past(request):
    if request.user.is_authenticated:
        code = Code.objects.filter(user_id=request.user.id)
        return render(request, 'website/past.html', {"code": code})
    else:
        messages.success(request, "You must be logged in to view this page")
        return redirect('home')


def delete_past(request, past_id):
    past = Code.objects.get(pk=past_id)
    past.delete()
    messages.success(request, "Deleted successfully")
    return redirect('past')
